
.. _font_awsome_django_1:

=====================================
Fontawsome Django use case 1
=====================================




Django Template file
======================

.. seealso::

   - https://www.w3schools.com/icons/fontawesome5_intro.asp


We prefer the KIT CODE approach. Once you get the code you can start
using Font Awesome on your web pages by including only one line of HTML
code::

    <script src=”https://kit.fontawesome.com/yourcode.js”></script>


.. yourcode

.. code-block:: html

    <!doctype html>
    <html>
    <head>
      <!-- Place your kit's code here -->
      <script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script>
    </head>

    <body>
      <i class="fas fa-thumbs-up fa-5x"></i>
    </body>
    </html>




.. code-block:: django

    {% block javascript_libraries %}
        {{ block.super }}
        <script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script>
    {% endblock javascript_libraries %}



    <th>
         <a href="?order=-agence__id_etude_notariale">
         <i class="fas fa-sort-alpha-down-alt fa-2x"></i></a>
         {% trans "Agence" %}
         <a href="?order=agence__id_etude_notariale">
         <i class="fa fa-sort-alpha-down fa-2x"></i></a>
    </th>
    <th>
         {# https://www.w3schools.com/icons/icons_reference.asp #}
         <a href="?order=-nb_transactions">
         <i class="fas fas fa-sort-amount-down fa-2x"></i></a>
         {% trans "Nb transactions" %}
         <a href="?order=nb_transactions">
         <i class="fa fas fa-sort-amount-down-alt fa-2x"></i></a>
    </th>


.. figure:: example_1.png
   :align: center


.. code-block:: django
   :linenos:


    <th>
    <a href="?order=-agence__id_etude_notariale">
             <i class="fas fa-sort-alpha-down-alt fa-2x"></i></a>
             {% trans "Agence" %}
             <a href="?order=agence__id_etude_notariale">
             <i class="fa fa-sort-alpha-down fa-2x"></i></a>
    </th>
    <th>
         <a href="?order=-nb_documents">
         <i class="fas fas fa-sort-amount-down fa-2x"></i></a>
         {% trans "Nb total documents" %}
         <a href="?order=nb_documents">
         <i class="fa fas fa-sort-amount-down-alt fa-2x"></i></a>
    </th>


.. figure:: example_2.png
   :align: center



views.py
===========

.. code-block:: python
   :linenos:

    try:
        order = request.GET.get("order", "-nb_transactions")
        logger.info(f"{order=}")
    except:
        order = "-nb_transactions"


    stats_agencies_year_month = (
        StatsMonthAgence.objects.filter(year=year, month=month)
        .order_by("agence__id_etude_notariale")
        .order_by(order)
        .prefetch_related("agence")
    )
