.. index::
   pair: Font ; awsome
   ! Font awsome


.. _font_awsome:

======================================================
**Font awsome**
======================================================

.. seealso::

   - https://fontawesome.com/
   - https://fontawesome.com/icons?d=gallery&m=free
   - https://github.com/FortAwesome/Font-Awesome
   - https://en.wikipedia.org/wiki/Font_Awesome


.. toctree::
   :maxdepth: 3

   definition/definition
   cheatsheet/cheatsheet
   icons/icons
   django/django
   versions/versions
