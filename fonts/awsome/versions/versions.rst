.. index::
   pair: Font awsome; Versions

.. _font_awsome_versions:

=====================================
Font awsome versions
=====================================

.. seealso::

   - https://github.com/FortAwesome/Font-Awesome/releases

.. toctree::
   :maxdepth: 3

   5/5
   4/4
