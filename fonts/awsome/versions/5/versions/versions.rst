
.. _fontawsome_5_versions:

===============================
**Font awsome 5** versions
===============================

.. toctree::
   :maxdepth: 3

   5.13.0/5.13.0
   5.12.1/5.12.1
