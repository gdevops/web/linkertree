.. index::
   pair: Font awsome; sort

.. _awsome_icons_sort:

=====================================
Font awsome icons **sort**
=====================================

.. seealso::

   - https://fontawesome.com/icons?d=gallery&m=free&q=sort




Icones "fas" (Awsome 5)
========================


.. figure:: icones_tri.png
   :align: center

   https://www.w3schools.com/icons/icons_reference.asp


::

    fas fa-sort-alpha-down  &#xf15d;
    fas fa-sort-alpha-down-alt  &#xf881;
    fas fa-sort-alpha-up    &#xf15e;
    fas fa-sort-alpha-up-alt    &#xf882;
    fas fa-sort-amount-down     &#xf160;
    fas fa-sort-amount-down-alt     &#xf884;
    fas fa-sort-amount-up   &#xf161;
    fas fa-sort-amount-up-alt   &#xf885;
    fas fa-sort-down


.. figure:: liste_agences_transactions.png
   :align: center
