.. index::
   pair: Font awsome; icons

.. _font_awsome_icons:

=====================================
Font awsome **icons**
=====================================

.. seealso::

   - https://www.w3schools.com/icons/icons_reference.asp (**Awsome 5, fas**)
   - https://www.w3schools.com/icons/fontawesome_icons_webapp.asp (**Awsome 4, fa**)
   - https://fontawesome.com/cheatsheet?from=io
   - https://www.w3schools.com/icons/icons_reference.asp
   - https://fontawesome.com/icons?d=gallery&m=free

.. toctree::
   :maxdepth: 3

   eye/eye
   link/link
   pencil/pencil
   sort/sort
