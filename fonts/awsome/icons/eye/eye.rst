.. index::
   pair: Font awsome; eye

.. _awsome_icons_eye:

=====================================
Font awsome icons **eye**
=====================================

.. seealso::

   - https://fontawesome.com/icons?d=gallery&m=free&q=eye




Example
========

.. seealso::

   - https://www.w3schools.com/icons/tryit.asp?filename=tryicons_awesome5_intro_basic3


fa (4.7.0) versions
======================

.. seealso::

   - https://www.w3schools.com/icons/fontawesome_icons_webapp.asp


.. figure:: fa_eye.png
   :align: center


Javascript code
----------------

::

    let x = document.getElementById("id_description_toggle_preview");
    if (x.title === "Write") {
        x.title = "Preview";
        x.classList.remove("fa-pencil");
        x.classList.add("fa-eye");
    } else {
        x.title = "Write";
        x.classList.remove("fa-eye");
        x.classList.add("fa-pencil");
    }
