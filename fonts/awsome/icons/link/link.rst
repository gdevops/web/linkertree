.. index::
   pair: Font awsome; link

.. _awsome_icons_link:

=====================================
Font awsome icons **link**
=====================================

.. seealso::

   - https://fontawesome.com/icons?d=gallery&q=link

