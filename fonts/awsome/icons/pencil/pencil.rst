.. index::
   pair: Font awsome; pencil

.. _awsome_icons_pencil:

=====================================
Font awsome icons **pencil**
=====================================

.. seealso::

   - https://fontawesome.com/icons?d=gallery&m=free&q=pencil





fa (4.7.0) versions
======================

.. seealso::

   - https://www.w3schools.com/icons/fontawesome_icons_webapp.asp


.. figure:: 4.7.0/fa_pencils.png
   :align: center

   https://www.w3schools.com/icons/fontawesome_icons_webapp.asp


Javascript code
----------------

::

    let x = document.getElementById("id_description_toggle_preview");
    if (x.title === "Write") {
        x.title = "Preview";
        x.classList.remove("fa-pencil");
        x.classList.add("fa-eye");
    } else {
        x.title = "Write";
        x.classList.remove("fa-eye");
        x.classList.add("fa-pencil");
    }
