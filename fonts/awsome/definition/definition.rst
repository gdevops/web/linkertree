.. index::
   pair: Font ; awsome
   ! Font awsome


.. _font_awsome_def:

=====================================
Font awsome definition
=====================================

.. seealso::

   - https://fontawesome.com/
   - https://github.com/FortAwesome/Font-Awesome
   - https://en.wikipedia.org/wiki/Font_Awesome





Definition
============

Font Awesome is a font and icon toolkit based on CSS and LESS.

It was made by Dave Gandy for use with Twitter Bootstrap, and later was
incorporated into the BootstrapCDN.

Font Awesome has a 38% market share among those websites which use third-party
Font Scripts on their platform, ranking it second place after Google Fonts.

Font Awesome 5 was released on December 7, 2017 with 1,278 icons.

Version 5 comes in two packages: Font Awesome Free and the proprietary Font
Awesome Pro (available for a fee).

The free versions (all releases up to 4 and the free version for 5) are available
under SIL Open Font License 1.1, Creative Commons Attribution 4.0, and MIT License.
