.. index::
   pair: Version ; rfs
   pair: rfs ; versions

.. _rfs_versions:

=====================================
Responsive Font sizes versions
=====================================

.. seealso::

   - https://github.com/twbs/rfs/releases

.. toctree::
   :maxdepth: 3

   8.0.4/8.0.4
