

.. _responsive_fonts_def:

=====================================
Responsive Fonts definition
=====================================

.. seealso::

   - https://github.com/twbs/rfs



Bootstrap 4.3.0
==================

Our biggest new addition to Bootstrap in v4.3 is responsive font sizes, a new
project in the Bootstrap GitHub org to automate calculate an appropriate
font-size based on the dimensions of a visitor’s device or browser viewport.

Here’s how it works:

- All font-size properties have been switched to the @include font-size() mixin.
  Our Stylelint configuration now prevents the usage of font-size property.
- Disabled by default, you can opt into this new behavior by toggling the
  $enable-responsive-font-sizes boolean variable.
- font-sizes are entirely configurable via Sass.
  Be sure to read the docs for how to modify the scales, variables, and more.

While responsive font-sizes are disabled by default, we’ve enabled them in the
custom CSS that powers our docs starting with v4.3.

**Please share feedback with us via GitHub issues or on Twitter.**

We’ve added some light guidance to our Typography docs to explain the feature.
You can also learn more by reading the rfs project documentation.
