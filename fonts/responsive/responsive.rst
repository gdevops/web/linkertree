.. index::
   pair: Responsive ; Font size
   ! Responsive Font sizes
   ! rfs

.. _responsive_font_sizes:
.. _rfs:

=====================================
Responsive Font sizes (rfs)
=====================================

.. seealso::

   - https://github.com/twbs/rfs
   - https://x.com/getbootstrap
   - https://getbootstrap.com/docs/4.3/content/typography/#responsive-font-sizes


.. figure:: rfs.svg
   :align: center
   :width: 300




README.md
===========

.. include:: README.rst

Definition
===========

.. toctree::
   :maxdepth: 3

   definition/definition


Versions
==========

.. toctree::
   :maxdepth: 3

   versions/versions
