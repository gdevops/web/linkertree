.. index::
   pair: Web ; Methods
   ! Methods

.. _web_methods:

========================
Methods
========================

.. seealso::

   - :ref:`web_javascript_technical_debt`





The lean web
==============

.. seealso::

   - :ref:`lean_web`


Small technology FOUNDATION
=============================

.. seealso::

   - :ref:`small_tech`


Web javascript technical debt
===============================

.. seealso::

   - :ref:`web_javascript_technical_debt`
