.. index::
   pair: Web API; Tutorials
   ! Web APIs

.. _web_apis_tutos:

=========================
Tutorials
=========================

- https://developer.mozilla.org/en-US/docs/Web/API
- https://developer.mozilla.org/fr/docs/Apprendre/JavaScript/Client-side_web_APIs/Introduction
- :ref:`genindex`
- :ref:`search`


.. toctree::
   :maxdepth: 6

   html_css_js/html_css_js
   mozilla/mozilla
