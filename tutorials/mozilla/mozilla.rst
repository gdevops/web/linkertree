
.. _mozilla_web_apis_tutos:

=================================
Mozilla **Web APIs** tutorials
=================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Learn
   - https://developer.mozilla.org/en-US/docs/Web/API
   - https://developer.mozilla.org/en-US/docs/Glossary
   - https://developer.mozilla.org/en-US/docs/Learn/Release_notes
   - https://developer.mozilla.org/fr/docs/Apprendre/JavaScript/Client-side_web_APIs/Introduction
   - https://github.com/mozfr/besogne/wiki/Matrix
   - :ref:`genindex`
   - :ref:`search`
